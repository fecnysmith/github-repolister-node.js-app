import React, {Component} from 'react';
import {registerObservable} from "./utils/observables";
import Description from "./views/description";
import Main from "./views/main";
import {BrowserRouter, Route, Switch} from "react-router-dom";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showLoading: true
        };
    }

    componentDidMount() {
        let that = this;
        registerObservable("setLoading", "setLoading", state => {
            that.setState({showLoading: state})
        })
    }

    loadingWrapper() {
        return <div className={"loading"}>
            <div className={"icon-wrapper"}>
                <i className="fab fa-github fa-5x fa-pulse"/>
            </div>
        </div>;
    }

    render() {
        const {showLoading} = this.state;
        return <section className={"page"}>
            {showLoading && this.loadingWrapper()}

            <div className={showLoading ? "page-container blur" : "page-container"}>
                <BrowserRouter>
                    <Switch>
                        <Route exact path='/' component={Main}/>
                        <Route exact path='/:GitHubUser' component={Main}/>
                        <Route path='/:GitHubUser/:repoName' component={Description}/>
                    </Switch>
                </BrowserRouter>
            </div>
        </section>;
    }
}

export default App;
