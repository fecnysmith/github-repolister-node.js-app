import React, {Component} from 'react';
import bindAll from 'lodash.bindall';
import GitHub from 'github-api';
import RepoRow from "./components/RepoRow";
import ReactPaginate from "react-paginate";
import {observ} from "../utils/observables";
import MainLead from "./components/MainLead";
import {GITHUB_TOKEN} from "../../config";

const gh = new GitHub({token: GITHUB_TOKEN});
class Main extends Component {

    constructor(props) {
        super(props);
        bindAll(this, 'getRepos', 'handlePageClick');

        this.state = {
            GitHubUser: this.props.match.params.GitHubUser,
            GitHubRepos: [],
            loading: false,
            pagination: {
                currentPage: 0,
                itemsPerPage: 3
            }
        };

    }

    componentDidMount(){
        if(this.props.match.params.GitHubUser){
            this.getRepos();
        }else{
            setTimeout(function(){observ("setLoading", false)},400);
        }
    }

    handlePageClick(event){
        let currentState = this.state.pagination;
        currentState.currentPage = Number(event.selected)
        this.setState({
            pagination: currentState
        });
    }

    getRepos(){
        let that = this;

        // set loading in App component and set an empty array as GitHubRepos state
        observ("setLoading", true);
        let paginationState = this.state.pagination;
        paginationState.currentPage = 0;
        that.setState({GitHubRepos: [], pagination: paginationState});

        // set github user
        let userData = gh.getUser(this.state.GitHubUser);

        // fetch repos...
        userData.listRepos({sort: "updated"}).then(response => {
            //...then set states
            that.setState({GitHubRepos: response.data});
            that.props.history.push("/"+this.state.GitHubUser);

            // disable loading in App component
            observ("setLoading", false);
        }).catch(response => {

            that.setState({GitHubRepos: ''});
            that.props.history.push("/");
            observ("setLoading", false);
        });
    }

    getPaginate(){
        return <nav aria-label="Page navigation example">
            <ReactPaginate previousLabel={<span className={"fas fa-angle-double-left"}/>}
                       nextLabel={<span className={"fas fa-angle-double-right"}/>}
                       breakLabel={<a href="">...</a>}
                       breakClassName={"break-me"}
                       pageCount={Math.round(this.state.GitHubRepos.length / this.state.pagination.itemsPerPage)}
                       marginPagesDisplayed={2}
                       pageRangeDisplayed={5}
                       onPageChange={this.handlePageClick}
                       containerClassName={"pagination justify-content-center"}
                           pageClassName={"page-item"}
                           pageLinkClassName={"page-link"}

                           previousLinkClassName={"page-link"}
                           nextLinkClassName={"page-link"}

                           previousClassName={"page-item"}
                           nextClassName={"page-item"}

                           activeClassName={"active"} /></nav>
    }

    static getPaginateOffset(pagination){
        pagination.startOffset = (pagination.currentPage === 0 ? 0 : pagination.currentPage * pagination.itemsPerPage + 1);
        pagination.endOffset = pagination.startOffset + pagination.itemsPerPage;
        return pagination;
    }

    render() {
        let that = this;

        // set pagination rule & offset
        const pagination = Main.getPaginateOffset(this.state.pagination);

        // get sliced list from GitHubRepos state
        const showRepos = this.state.GitHubRepos.slice(pagination.startOffset, pagination.endOffset);

        return  <div>
            <MainLead MainState={this.state} fetchList={username => {that.setState({GitHubUser: username}, that.getRepos)}}/>
            {this.state.GitHubRepos.length > 0 ?
                <section>
                    <ul>{showRepos.map(repo => {
                            return <RepoRow key={repo.id} repo={repo} clickable={true}/>})}
                            </ul>

                    {this.state.GitHubRepos.length > this.state.pagination.itemsPerPage && this.getPaginate()}

                </section> :
                (this.state.GitHubUser && <h3 className={"lead text-center mt-4"}>No public repository found!</h3>)
            }
        </div>;
    }
}

export default Main;
