import React, {Component} from 'react';
import GitHub from "github-api";
import RepoRow from "./components/RepoRow";
import Markdown from 'react-markdown';
import {observ} from "../utils/observables";
import RepoContents from "./components/RepoContents";
import {GITHUB_TOKEN} from "../../config";


const gh = new GitHub({token: GITHUB_TOKEN});


class Description extends Component {
    constructor(props) {
        super(props);
        this.state = {
            repoData: false,
            readMe: false,
            repoContents: false
        };
    }


    componentWillMount() {
        let that = this;

        let repository = gh.getRepo(this.props.match.params.GitHubUser, this.props.match.params.repoName);
        repository.getDetails().then(response => {
            that.setState({repoData: response.data})
        });


        repository.getContents(null, null, true).then(response => {
            that.setState({repoContents: response.data})
        });


        repository.getReadme(null, true)
            .then(response => {
                that.setState({readMe: response.data}, e => {
                    observ("setLoading", false)
                })
            })
            .catch(response => {
                that.setState({readMe: false}, e => {
                    observ("setLoading", false)
                })
            });
    }


    render() {
        return this.state.repoData ?
            <section>
                <RepoRow repo={this.state.repoData} clickable={false}/>
                <div className={"row col-md-12 mt-4"}>
                    {this.state.repoContents && <RepoContents repoContents={this.state.repoContents}/>}
                </div>

                <div className={"col-md-12 mt-4 border-top pt-3"}>
                    {this.state.readMe ? <Markdown source={this.state.readMe}/> :
                        <h3 className={"lead text-center mt-4"}>No Read me! file added yet!</h3>}
                </div>
            </section>
            : <div/>;
    }
}
export default Description;
