import React, {Component} from 'react';

class MainLead extends Component {
    render() {
        const {MainState, fetchList} = this.props;
        const mainClass = (!MainState.GitHubUser && !MainState.GitHubRepos.length > 0 ? "main-lead initial text-center" : "main-lead text-center border-bottom");

        return <div className={mainClass}>
            <h2>GitHub repo lister for ingatlan.com</h2>
            <div className={"lead"}>
                Enter the GitHub username to list public repositories then press enter
                <div className={"row justify-content-center my-3"}>
                    <div className={"col-md-4"}>
                        <input type={"text"}
                               className={"form-control "}
                               defaultValue={MainState.GitHubUser}
                               placeholder={"GitHub username"}
                               onKeyPress={event => {
                                   if (event.key === 'Enter')
                                       fetchList(event.target.value);
                               }}/>
                    </div>
                </div>
            </div>
        </div>;
    }
}

export default MainLead;
