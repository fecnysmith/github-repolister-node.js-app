import React, {Component} from 'react';
import TimeAgo from 'react-timeago'
import {observ} from "../../utils/observables";
import {Link} from "react-router-dom";


class RepoRow extends Component {
    render() {
        const {repo} = this.props;
        return <li className="col-12 d-block width-full border-bottom mt-6 py-4">

            <div className="d-inline-block mb-1 row col-md-12">
                {this.props.clickable ?
                    <h3><Link to={'/' + repo.full_name} className={"repo-row"} onClick={function () {
                        observ("setLoading", true)
                    }}>{repo.name}</Link></h3>
                    :
                    <h3>{repo.name}
                        <Link to={'/' + repo.owner.login} className={"float-right"}>
                            {repo.owner.login}
                            <img src={repo.owner.avatar_url} className={"img-fluid img-thumbnail rounded-circle"}
                                 width={"27px"} style={{marginTop: '-3px'}}/>
                        </Link>
                    </h3>
                }
            </div>
            <div><p className="row col-12 d-inline-block text-gray mb-2">{repo.description}</p></div>

            <div className="f6 text-gray mt-2">

                {repo.language && <span className="mr-3">
                            <i className="fas fa-code"/>
                    {repo.language}
                        </span>}
                <span className="mr-3">
                            <i className="fas fa-star"/>
                    {repo.stargazers_count}
                        </span>
                <span className="mr-3">
                            <i className="fas fa-code-branch"/>
                    {repo.forks_count}
                        </span>
                {repo.license && <span className="mr-3">
                            <i className="fas fa-balance-scale"/>
                    {repo.license.spdx_id}
                        </span>}
                <span className="mr-3">
                            {repo.open_issues_count} issues need help
                        </span>

                <span className={"float-right"}>Updated <TimeAgo date={repo.updated_at}/></span>
            </div>
        </li>;
    }
}
export default RepoRow;
