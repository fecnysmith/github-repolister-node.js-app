import React, {Component} from 'react';
import filesize from "filesize";


class RepoContents extends Component {

    orderRepoContents(repoContents) {
        const getSort = function (property) {
            let sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                const result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        };

        return repoContents.sort(getSort('type'));
    }

    render() {
        const repoContents = this.orderRepoContents(this.props.repoContents);
        return <ul className={"repo-contents"}>
            {repoContents.map(thisRow => {
                return <li key={thisRow.name}>
                    <i style={{"width": "20px"}}
                       className={thisRow.type === "dir" ? "fas fa-folder-open" : "fas fa-file-alt"}/>
                    <span>{thisRow.name}</span>
                    {thisRow.size > 0 && <span className={"float-right"}>{filesize(thisRow.size)}</span>}
                </li>
            })}
        </ul>;
    }
}

export default RepoContents;
