import React from 'react';
import {render} from 'react-dom';
import '../scss/styles.scss';
import App from "../src/App";

render(<App/>, document.getElementById('app'));