const path = require('path');
const express = require('express');
const history = require('connect-history-api-fallback');
const logger = require('morgan');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const PORT = process.env.PORT || 9000;

app.use(history());
if (process.env.NODE_ENV === 'development') {
    console.log('Starting development server on port ', PORT);
    const webpackDevMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');
    const webpack = require('webpack');
    const config = require('./webpack.config.js');
    const compiler = webpack(config);

    app.use(webpackDevMiddleware(compiler, {noInfo: true, publicPath: config.output.publicPath}));
    app.use(webpackHotMiddleware(compiler));
    app.use(logger('dev'));
} else {
    console.log('Starting production server on port ', PORT);
}

app.use('/', express.static('public'));

const urlencodedParser = bodyParser.urlencoded({extended: false});

app.listen(PORT, function (error) {
    if (error) {
        console.error(error);
    } else {
        console.info("Listening on port %s. Visit http://localhost:%s/ in your browser.", PORT, PORT);
    }
});
